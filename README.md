# launchdarkly-client-unity-plugin
Leverage the LaunchDarkly Client SDK in Unity with ease using this plugin.

Please note that this plugin assumes the Unity project is set to use .NET API compatibility level 4.x (Project Settings -> Player -> Configuration -> API Compatibility Level).

This project is not officially supported by LaunchDarkly or Unity.
